package main

import (
	"fmt"
	"os"
	"strconv"
)

func main() {

	var osArgsArray = make([]int,0)

	for _,v := range os.Args[2:] {
		value,error := strconv.Atoi(v)

		if error != nil {
			continue
		}
		osArgsArray = append(osArgsArray, value)
	}

	fmt.Println(osArgsArray)

	switch os.Args[1] {
	case "bubble":
		bubbleSort(osArgsArray)
	case "gnome":
		gnomeSort(osArgsArray)
	case "selection":
		selectionSort(osArgsArray)
	default:
		break
	}
}

func bubbleSort(array []int) []int {
	for i := 0; i < len(array); i++ {
		for j := 1; j < len(array)-i; j++ {
			if array[j] < array[j-1] {
				array[j], array[j-1] = array[j-1], array[j]
			}
		}
	}
	fmt.Println(array)
	return array
}

func gnomeSort(array []int) []int {
	for i, j := 1, 2; i < len(array); {
		if array[i-1] > array[i] {
			array[i-1], array[i] = array[i], array[i-1]
			i--
			if i > 0 {
				continue
			}
		}
		i = j
		j++
	}
	fmt.Println(array)
	return array
}

func selectionSort(array []int) []int {
	for i := 0; i < len(array); i++ {
		minIndex := i

		for j := i + 1; j < len(array); j++ {
			if array[j] < array[minIndex] {
				minIndex = j
			}
		}
		array[minIndex], array[i] = array[i], array[minIndex]
	}
	fmt.Println(array)
	return array
}
